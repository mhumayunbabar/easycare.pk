Rails.application.routes.draw do

  get 'public/sign_in'
  get 'public/home'
  get 'public/contact_us'
  get 'public/coming_soon'
  get 'public/doctors'
  get 'public/doctor_details'
  get 'zohoverify/verifyforzoho' => 'public#verifyforzoho'

  get 'public/search'
  post 'public/search_doctors'

  get 'public/open_appointment'
  post 'public/open_appointment'

  post 'public/login'

  get 'public/contact'
  get 'public/about'

  get 'public/doctor_registration'
  post 'public/register_doctor'
  
  post 'public/contact_form'
  post 'public/register_donor'
  post 'public/register_request_donation'
  post 'public/place_order'

  get 'public/privacy'
  get 'public/donor_registration'
  get 'public/request_donation'

  get 'public/healthy_living'
  get 'public/healthy_living/:id' => 'public#blog_details'

  get 'public/help'
  get 'public/show_location'
  get 'public/pharmacy'
  get 'public/add_medicine'
  get 'public/medicine_details'
  get 'public/checkout'

  get 'public/get_slots'

  get 'public/patient_registration'
  post 'public/register_patient'

  get 'public/appointment'
  post 'public/book_appointment'

  post 'public/search_medicine'

  get 'public/thank_you'


  get 'public/terms_and_conditions'

  get 'welcome/index'

  get 'welcome/about'
  get 'welcome/login'

  get 'welcome/help'

  get 'welcome/faqs'

  get 'welcome/contact_us'

  get 'welcome/index'

  get 'welcome/about'

  get 'welcome/help'

  get 'welcome/faqs'
  
  get 'api/get_patients'
  get 'api/get_appointments'
  get 'api/get_medicines'
  get 'api/get_orders'
  get 'api/get_stock'

  get 'api/verify_email'
  



  get 'admin' => "admins#dashboard"

  resources :pharmacies do
    collection do
      get 'login'
      get 'dashboard'
      get 'logout'
      get 'destroy_session'
      post 'session_login'
    end
    member do
      get 'orders'
      get 'set_status'

      get 'stock'
      get 'add_stock'
      post 'add_new_stock'
    end
  end

  resources :patients do 
    collection do
      get 'login'
      get 'logout'
      get 'dashboard'
      get 'destroy_session'
      post 'session_login'
    end
    member do
      get 'appointments'
      get 'request_appointment'
      post 'create_new_appointment'

      get 'get_slots'

      get 'prescriptions'
      get 'prescription_details'
    end
  end

  resources :bloggers do
    collection do
      get 'login'
      get 'logout'
      get 'dashboard'
      get 'destroy_session'
      post 'session_login'
    end
    member do
      get 'new_article'
      post 'create_new_article'
    end
  end

  resources :admins do 
    collection do
      get 'login'
      get 'logout'
      get 'dashboard'
      get 'procedures'
      get 'specialities'
      get 'conditions'
      get 'new_blood_group'
      get 'new_procedure'
      get 'new_condition'
      get 'destroy_session'
      get 'new_speciality'

      get 'hospitals'
      get 'cities'
      get 'new_hospital'
      get 'new_city'
      get 'donation_requests'
      get 'assign_donation_request'
      post 'assign_donor'

      get 'orders'
      get 'order_details'
      post 'assign_order'

      get 'new_blogger'
      post 'create_new_blogger'

      get 'medicines'
      get 'edit_medicine'
      get 'delete_medicine'
      post 'edit_medicine_data'

      get 'new_medicine'
      post 'create_new_medicine'
      get 'import_medicines'
      post 'import_medicines_data'

      get 'blogs'
      get 'publish_blog'
      get 'blog_content'

      post 'add_new_hospital'
      post 'add_new_city'
      post 'add_new_blood_group'
      post 'create_new_procedure'
      post 'create_new_speciality'
      post 'create_new_condition'
      post 'session_login'

      get 'edit_location'
      post 'edit_hospital_location'

      get 'pharmacies'

      get 'donors'
    end
  end

  resources :users do
     collection do
      get 'dashboard'
      post 'session_login'
    end
  end
  
resources :doctors do
     collection do
      get 'login'
      get 'logout'
      get 'dashboard'
      get 'destroy_session'
      post 'create_appointment' 
      post 'session_login'
    end
    member do
      get 'patients'
      get 'appointments'
      get 'via_n_appointments'
      get 'new_appointment'
      get 'new_stub_appointment'
      post 'add_new_stub_appointment'

      get 'availability_settings'
      post 'set_availability_details'
      get 'get_slots'

      get 'procedures'
      get 'add_procedure'
      post 'add_new_procedure'

      get 'conditions'
      get 'new_condition'
      post 'add_new_condition'

      get 'specialities'
      get 'add_speciality'
      post 'add_new_speciality'

      get 'prescriptions'
      get 'new_prescription'
      post 'add_new_prescription'
      get 'prescription_details'

      get 'settings'
      post 'update_information'

      get 'change_picture'
      post 'add_new_picture'

      get 'accept_appointment'
      get 'reject_appointment'

      get 'mark_paid'
      post 'mark_appointment_paid'
    end
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'public#healthy_living'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
