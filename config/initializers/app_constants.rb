PAYMENT_METHODS = {
:cash_on_delivery => 1,
:easy_paisa => 2 
}
ORDER_STATUS = {
:pending => 0,
:assigned => 1,
:delivered => 2 
}
BLOG_STATUS = {
:pending => 0,
:published => 1,
:rejected => 2
}