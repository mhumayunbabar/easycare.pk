/**
 * Form wizard demo
 */
 $(document).ready(function(){
(function ($) {
  'use strict';

  // Credit card form
  $('#wizardForm').card({
    container: '.credit-card'
  });

  // Checkbo plugin
  $('.checkbo').checkBo();

  // Jquery validator
  var $validator = $('#wizardForm').validator();

  function checkValidation() {
    var hasErrors  = $('#wizardForm').validator('validate').has('.has-error').length
    if (hasErrors) return false;
  }

  // Twitter bootstrap wizard
  $('#rootwizard').bootstrapWizard({
    tabClass: '',
    'nextSelector': '.button-next',
    'previousSelector': '.button-previous',
    onNext: checkValidation,
    onLast: checkValidation,
    onTabClick: checkValidation
  });
})(jQuery);
  
});