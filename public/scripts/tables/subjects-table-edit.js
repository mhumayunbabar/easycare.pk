/**
 * Datatables editable demo page
 */
(function ($) {
  'use strict';

  var nEditing = null,
    oTable, perData;

  oTable = $('.datatable').dataTable({
     "paging":   false,
      "ordering": false,
      ajax: '/api/get_subjects',
      "columns": [
            { "data": "subject"},
            {"data": "level"}
            ],
       "createdRow": function ( row, data, index ) {
          $(row).append('<td><a href="/grades/add_test?id='+data.id+'" class="btn btn-info" data-remote="true"> <i class="fa fa-edit"></i> Add Test</a></td>');
          $(row).append('<td><a href="/grades/view_tests?id='+data.id+'" class="btn btn-info" data-remote="true"> <i class="fa fa-edit"></i> View Test</a></td>');
        }              
  });

})(jQuery);