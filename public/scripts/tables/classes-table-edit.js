/**
 * Datatables editable demo page
 */
(function ($) {
  'use strict';

  var nEditing = null,
    oTable, perData;

  oTable = $('.datatable').dataTable({
     "paging":   false,
      "ordering": false,
      ajax: '/api/get_classes',
      "columns": [
            { "data": "level"},
            {"data": "sections_count"},
            {"data": "no_of_students"},
            {"data": "link_to_sections"}
            ],
     "createdRow": function ( row, data, index ) {
          $(row).append('<a href="javascript:;" class="delete">Delete</a></td>')
        }
                
  });

  $('.datatable').on('click', 'a.delete', function (e) {
    e.preventDefault();
    var nRow = $(this).parents('tr')[0];
    var aData = oTable.fnGetData(nRow);

        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this class, related sections.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, delete this class.',
          cancelButtonText: 'No, cancel please.',
          closeOnConfirm: false,
          closeOnCancel: false
        }, function (isConfirm) {
          if (isConfirm) {
            swal.close();
             $.ajax({
                  type: "POST",
                  url: "/api/delete_class",
                  data: { "id": aData["id"]},
                  dataType: "json",
                  success: function( response ) {
                     oTable.fnDeleteRow(nRow);
                     swal('Deleted!', response.alert, "success");
                  },
                  error: function(xhr, status, error) {
                    // console.log("error while searching item...");
                  }
                });
          } else {
            swal('Cancelled', 'Class Is safe :) ', 'error');
            // setTimeout(function(){ swal.close();  }, 1000);  
          }
        });
      
    
   
  });

})(jQuery);