/**
 * Datatables editable demo page
 */
  var nEditing = null,
    oTable;
(function ($) {
  'use strict';


  function restoreRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    oTable.fnUpdate(aData["patient"], nRow, 0, false);
    oTable.fnUpdate(aData["email"], nRow, 1, false);
    oTable.fnUpdate(aData["contact"], nRow, 2, false);
    oTable.fnUpdate(aData["username"], nRow, 3, false);
    jqTds[4].innerHTML = '<a class="edit btn btn-info" href=\'\'><i class="fa fa-edit"></i> Edit</a>';
    oTable.fnDraw();
  }

  function editRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["name"] + '\'>';
    jqTds[1].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["email"] + '\'>';
    jqTds[2].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["contact"] + '\'>';
    jqTds[3].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["username"] + '\'>';
    jqTds[4].innerHTML = '<a class="edit btn btn-success" href=\'\'><i class="fa fa-save"></i> Save</a>';
    $(nRow).data('id',aData["id"]);
  }

  function saveRow(oTable, nRow) {
    var jqInputs = $('input', nRow);
    var jqTds = $('>td', nRow);
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
    oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
    jqTds[4].innerHTML = '<a class="edit btn btn-info" href=\'\'><i class="fa fa-edit"></i> Edit</a>';
    oTable.fnDraw();
    $.ajax({
        type: "POST",
        url: "/api/update_student",
        data: { "id": $(nRow).data('id'),
          "name": jqInputs[0].value,
          "email": jqInputs[1].value,
          "contact": jqInputs[2].value,
          "username": jqInputs[3].value
          },
        dataType: "json",
        success: function( response ) {
          new PNotify({
              type: response.type,
              title: response.title,
              text: response.alert,
              syling: "fontawesome",
              cornerclass: 'ui-pnotify-sharp',
              delay: 3000
          });

        },
        error: function(xhr, status, error) {
          // console.log("error while searching item...");
        }
      });
  }

  var path = $("#appointments_path").val();

  oTable = $('.datatable').dataTable({
      "processing": true,
      "serverSide": true,
      ajax: path,
      "columns": [
            { "data": "patient.name" },
            { "data": "patient.email" },
            { "data": "patient.phone" },
            { "data": "get_state" },
            { "data": "get_time" }
            ],
     "createdRow": function ( row, data, index ) {
      //console.log(row);
      //console.log(data);
          //$(row).append('<td><a href="javascript:;" class="btn btn-info edit"> <i class="fa fa-edit"></i> Edit</a></td><td><a href="javascript:;" class="delete btn btn-danger"><i class="fa fa-remove"></i> Delete</a></td>')
        }
                
  });

  // $('.toolbar').append('<a id=\'new\' href=\'javascript:;\' class=\'btn btn-info m-l\'  data-toggle=\'modal\' data-target=\'.bs-modal-sm\'>Add new row</a>');

  $('#new').on('click', function (e) {
    e.preventDefault();
    var aiNew = oTable.fnAddData(['', '', '', '', '', '<a class="edit btn btn-info" href=\'\'><i class="fa fa-edit"></i> Edit</a>', '<a class=\'delete\' href=\'\'><i class="fa fa-remove"></i> Delete</a>']);
    var nRow = oTable.fnGetNodes(aiNew[0]);
    editRow(oTable, nRow);
    nEditing = nRow;
  });

  $('.datatable').on('click', 'a.delete', function (e) {
    e.preventDefault();

    var nRow = $(this).parents('tr')[0];
    var aData = oTable.fnGetData(nRow);
    
        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this student.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, delete this student.',
          cancelButtonText: 'No, cancel please.',
          closeOnConfirm: false,
          closeOnCancel: false
        }, function (isConfirm) {
          if (isConfirm) {
            swal.close();
              $.ajax({
                  type: "POST",
                  url: "/api/delete_student",
                  data: { "id": aData["id"]},
                  dataType: "json",
                  success: function( response ) {
                    // response.type
                    // response.title
                    oTable.fnDeleteRow(nRow);
                    swal('Deleted!', response.alert, "success");
                  },
                  error: function(xhr, status, error) {
                  }
                });

          } else {
            swal('Cancelled', 'Student Is safe :) ', 'error');
            // setTimeout(function(){ swal.close();  }, 1000);  
          }
        });
      
        
  });

  $('.datatable').on('click', 'a.edit', function (e) {
    e.preventDefault();
    var nRow = $(this).parents('tr')[0];
    if (nEditing !== null && nEditing !== nRow) {
      restoreRow(oTable, nEditing);
      editRow(oTable, nRow);
      nEditing = nRow;
    } else if (nEditing === nRow && this.innerHTML === '<i class="fa fa-save"></i> Save') {
      saveRow(oTable, nEditing);
      nEditing = null;
    } else {
      editRow(oTable, nRow);
      nEditing = nRow;
    }
    return false;
  });
})(jQuery);