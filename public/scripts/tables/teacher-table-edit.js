/**
 * Datatables editable demo page
 */

  var nEditing = null,
    oTable;
(function ($) {
  'use strict';

  function restoreRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    oTable.fnUpdate(aData["name"], nRow, 0, false);
    oTable.fnUpdate(aData["email"], nRow, 1, false);
    oTable.fnUpdate(aData["user_name"], nRow, 2, false);
    oTable.fnUpdate(aData["emergency_contact"], nRow, 2, false);
    jqTds[3].innerHTML = '<a class="edit btn btn-info" href=\'\'><i class="fa fa-edit"></i> Edit</a>';
    oTable.fnDraw();
  }

  function editRow(oTable, nRow) {
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    jqTds[0].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["name"] + '\'>';
    jqTds[1].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["email"] + '\'>';
    jqTds[2].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["user_name"] + '\'>';    
    jqTds[3].innerHTML = '<input type=\'text\' class=\'form-control\' value=\'' + aData["emergency_contact"] + '\'>';
    jqTds[4].innerHTML = '<a class="edit btn btn-success" href=\'\'><i class="fa fa-save"></i> Save</a>';
    $(nRow).data('id',aData["id"]);
  }

  function saveRow(oTable, nRow) {
    var jqInputs = $('input', nRow);
    var jqTds = $('>td', nRow);
    oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
    oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
    oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
    oTable.fnUpdate(jqInputs[3].value, nRow, 2, false);
    jqTds[3].innerHTML = '<a class="edit btn btn-info" href=\'\'> <i class="fa fa-edit"></i> Edit</a>';
    oTable.fnDraw();
    $.ajax({
        type: "POST",
        url: "/api/update_teacher",
        data: { "id": $(nRow).data('id'),
          "name": jqInputs[0].value,
          "email": jqInputs[1].value,
          "user_name": jqInputs[2].value
          "emergency_contact": jqInputs[2].value
          },
        dataType: "json",
        success: function( response ) {
          new PNotify({
              type: response.type,
              title: response.title,
              text: response.alert,
              syling: "fontawesome",
              cornerclass: 'ui-pnotify-sharp',
              delay: 3000
          });

        },
        error: function(xhr, status, error) {
          // console.log("error while searching item...");
        }
      });
  }

  oTable = $('.datatable').dataTable({
      "processing": true,
      "serverSide": true,
      ajax: '/api/get_teachers',
      "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "user_name" },
            { "data": "emergency_contact" }],
     "createdRow": function ( row, data, index ) {
          $(row).append('<td><a href="javascript:;" class="edit btn btn-info"><i class="fa fa-edit"></i> Edit</a></td><td><a href="javascript:;" class="delete btn btn-danger"><i class="fa fa-remove"></i> Delete</a></td>')
        }
                
  });

  // $('.toolbar').append('<a id=\'new\' href=\'javascript:;\' class=\'btn btn-info m-l\'>Add new row</a>');

  $('#new').on('click', function (e) {
    e.preventDefault();
    var aiNew = oTable.fnAddData(['', '', '', '', '', '<a class="edit btn btn-info" href=\'\'><i class="fa fa-edit"></i> Edit</a>', '<a class="delete btn btn-danger" href=\'\'><i class="fa fa-remove"></i> Delete</a>']);
    var nRow = oTable.fnGetNodes(aiNew[0]);
    editRow(oTable, nRow);
    nEditing = nRow;
  });

  $('.datatable').on('click', 'a.delete', function (e) {
    e.preventDefault();
    var nRow = $(this).parents('tr')[0];
    var aData = oTable.fnGetData(nRow);

        swal({
          title: 'Are you sure?',
          text: 'You will not be able to recover this teacher.',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes, delete this teacher.',
          cancelButtonText: 'No, cancel please.',
          closeOnConfirm: false,
          closeOnCancel: false
        }, function (isConfirm) {
          if (isConfirm) {
            swal.close();
             $.ajax({
                  type: "POST",
                  url: "/api/delete_teacher",
                  data: { "id": aData["id"]},
                  dataType: "json",
                  success: function( response ) {
                     oTable.fnDeleteRow(nRow);
                     swal('Deleted!', response.alert, "success");
                  },
                  error: function(xhr, status, error) {
                    // console.log("error while searching item...");
                  }
                });
          } else {
            swal('Cancelled', 'Teacher Is safe :) ', 'error');
            // setTimeout(function(){ swal.close();  }, 1000);  
          }
        });
      
    
   
  });

  $('.datatable').on('click', 'a.edit', function (e) {
    e.preventDefault();
    var nRow = $(this).parents('tr')[0];
    if (nEditing !== null && nEditing !== nRow) {
      restoreRow(oTable, nEditing);
      editRow(oTable, nRow);
      nEditing = nRow;
    } else if (nEditing === nRow && this.innerHTML === '<i class="fa fa-save"></i> Save') {
      saveRow(oTable, nEditing);
      nEditing = null;
    } else {
      editRow(oTable, nRow);
      nEditing = nRow;
    }
    return false;
  });
})(jQuery);