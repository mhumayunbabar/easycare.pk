$(document).ready(function(){

  $(document).on('click', ".add_another_section", function(e){
     var target = $(e.target);
     target.removeClass('add_another_section');
     target.removeClass('btn-primary');
     target.addClass('btn-danger');
     target.addClass('remove_section');
     target.html('x');
     var div = ' <div class="form-group"> \
        <label class="col-sm-2 control-label">Section</label> \
        <div class="col-sm-6"> \
         <div class="input-group m-b"> \
           <input type="text" name="sections[]" id="sections_" placeholder="Enter Name" class="form-control br0 valid" required="required" aria-required="true" aria-invalid="false"> \
            <span class="input-group-btn"> \
              <button class="btn btn-primary add_another_section" type="button">Add Another Section</button> \
              </span>\
            </div> \
            </div> \
          </div>'
     $('.sections_div').append(div);
    });

  $(document).on('click', ".remove_section", function(e){
     var target = $(e.target);
     target.parents('.form-group').remove();
  });

});
