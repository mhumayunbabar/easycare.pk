class AddStatusToDonationRequest < ActiveRecord::Migration
  def change
    add_column :donation_requests, :status, :integer, :default => 0
  end
end
