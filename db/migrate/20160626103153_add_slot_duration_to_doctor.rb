class AddSlotDurationToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :slot_duration, :string
  end
end
