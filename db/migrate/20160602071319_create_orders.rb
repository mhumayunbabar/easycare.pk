class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :charges
      t.text :note
      t.string :address

      t.timestamps null: false
    end
  end
end
