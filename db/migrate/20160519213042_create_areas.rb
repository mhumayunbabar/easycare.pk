class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.string :name
      t.decimal :lat
      t.decimal :long

      t.timestamps null: false
    end
  end
end
