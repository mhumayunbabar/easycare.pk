class AddColumnsToMedicine < ActiveRecord::Migration
  def change
    add_column :medicines, :item_code, :integer
    add_column :medicines, :state, :integer
    add_column :medicines, :purchase_price, :float
    add_column :medicines, :manufacturer_name, :string
  end
end
