class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :item_id
      t.string :item_type

      t.timestamps null: false
    end
  end
end
