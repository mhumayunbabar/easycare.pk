class CreateHospitals < ActiveRecord::Migration
  def change
    create_table :hospitals do |t|
      t.string :name
      t.decimal :lat
      t.decimal :long
      t.integer :city_id

      t.timestamps null: false
    end
  end
  
end
