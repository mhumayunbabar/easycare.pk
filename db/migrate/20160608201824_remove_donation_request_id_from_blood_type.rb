class RemoveDonationRequestIdFromBloodType < ActiveRecord::Migration
  def change
    remove_column :blood_types, :donation_request_id, :integer
  end
end
