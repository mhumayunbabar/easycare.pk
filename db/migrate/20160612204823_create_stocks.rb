class CreateStocks < ActiveRecord::Migration
  def change
    create_table :stocks do |t|
      t.integer :pharmacy_id
      t.integer :medicine_id
      t.integer :amount

      t.timestamps null: false
    end
  end
end
