class AddOrderItemIdToMedicine < ActiveRecord::Migration
  def change
    add_column :medicines, :order_item_id, :integer
  end
end
