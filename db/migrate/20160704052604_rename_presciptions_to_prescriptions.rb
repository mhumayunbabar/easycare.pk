class RenamePresciptionsToPrescriptions < ActiveRecord::Migration
  def self.up
    rename_table :presciptions, :prescriptions
  end

  def self.down
    rename_table :prescriptions, :presciptions
  end
end
