class AddAreaIdToDonor < ActiveRecord::Migration
  def change
    add_column :donors, :area_id, :integer
  end
end
