class AddStateToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :state, :integer, default: 0
  end
end
