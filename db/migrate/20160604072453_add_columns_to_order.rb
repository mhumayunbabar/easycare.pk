class AddColumnsToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :area_id, :integer
    add_column :orders, :pharmacy_id, :integer
  end
end

