class AddDonationRequestIdToBloodType < ActiveRecord::Migration
  def change
    add_column :blood_types, :donation_request_id, :integer
  end
end
