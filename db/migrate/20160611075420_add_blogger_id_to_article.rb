class AddBloggerIdToArticle < ActiveRecord::Migration
  def change
    add_column :articles, :blogger_id, :integer
  end
end
