class CreateDoctorProcedures < ActiveRecord::Migration
  def change
    create_table :doctor_procedures do |t|
      t.integer :doctor_id
      t.integer :procedure_id

      t.timestamps null: false
    end
  end
end
