class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.decimal :amount
      t.text :note
      t.boolean :paid, default: false
      t.string :subject
      t.integer :account_id

      t.timestamps null: false
    end
  end
end
