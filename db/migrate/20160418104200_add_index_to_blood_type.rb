class AddIndexToBloodType < ActiveRecord::Migration
  def change
    add_index :blood_types, :title, unique: true
  end
end
