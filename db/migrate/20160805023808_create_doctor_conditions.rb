class CreateDoctorConditions < ActiveRecord::Migration
  def change
    create_table :doctor_conditions do |t|
      t.integer :doctor_id
      t.integer :condition_id

      t.timestamps null: false
    end
  end
end
