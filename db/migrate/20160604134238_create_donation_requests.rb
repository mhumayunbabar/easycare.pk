class CreateDonationRequests < ActiveRecord::Migration
  def change
    create_table :donation_requests do |t|
      t.string :blood_type
      t.string :name
      t.string :city
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
  end
end
