class AddCityIdToDonor < ActiveRecord::Migration
  def change
    add_column :donors, :city_id, :integer
  end
end
