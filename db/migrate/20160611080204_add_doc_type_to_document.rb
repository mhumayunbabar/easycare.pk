class AddDocTypeToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :doc_type, :integer, default: 0
  end
end
