class AddCityIdToDonationRequest < ActiveRecord::Migration
  def change
    add_column :donation_requests, :city_id, :integer
  end
end
