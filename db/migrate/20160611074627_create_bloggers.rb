class CreateBloggers < ActiveRecord::Migration
  def change
    create_table :bloggers do |t|
      t.string :name
      t.string :email
      t.string :password
      t.string :username

      t.timestamps null: false
    end
  end
end
