class AddSessionsTable < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :session_id,:limit => 100, :null => false
      t.text :data
      t.timestamps
    end

    add_index :sessions, :session_id, :unique => true, :length => {:session_id => 100}
    add_index :sessions, :updated_at
  end
end
