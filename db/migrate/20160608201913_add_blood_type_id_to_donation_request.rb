class AddBloodTypeIdToDonationRequest < ActiveRecord::Migration
  def change
    add_column :donation_requests, :blood_type_id, :integer
  end
end
