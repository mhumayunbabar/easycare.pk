class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.decimal :balance
      t.integer :owner_id
      t.string :owner_type

      t.timestamps null: false
    end
  end
end
