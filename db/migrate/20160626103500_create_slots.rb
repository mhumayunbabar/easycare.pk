class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.datetime :start_time
      t.integer :appointment_id
      t.integer :doctor_id

      t.timestamps null: false
    end
  end
end
