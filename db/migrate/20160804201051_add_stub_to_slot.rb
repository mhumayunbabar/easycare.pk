class AddStubToSlot < ActiveRecord::Migration
  def change
    add_column :slots, :stub, :boolean, default: false
  end
end
