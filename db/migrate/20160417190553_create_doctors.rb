class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.integer :gender
      t.date :date_of_birth
      t.string :email
      t.string :username
      t.string :password
      t.string :phone
      t.string :address

      t.timestamps null: false
    end
    add_index :doctors, :email, unique: true
    add_index :doctors, :username, unique: true
  end
end
