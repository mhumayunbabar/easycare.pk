class RemovePharmacyIdFromMedicine < ActiveRecord::Migration
  def change
    remove_column :medicines, :pharmacy_id, :integer
  end
end
