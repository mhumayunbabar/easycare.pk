class AddPharmacyIdToMedicines < ActiveRecord::Migration
  def change
    add_column :medicines, :pharmacy_id, :integer
  end
end
