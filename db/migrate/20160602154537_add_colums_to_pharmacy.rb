class AddColumsToPharmacy < ActiveRecord::Migration
  def change
    add_column :pharmacies, :email, :string
    add_column :pharmacies, :password, :string
    add_column :pharmacies, :username, :string
  end
end
