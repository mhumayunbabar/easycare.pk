class CreateAvailableDays < ActiveRecord::Migration
  def change
    create_table :available_days do |t|
      t.string :day
      t.datetime :start_time
      t.datetime :end_time
      t.integer :doctor_id

      t.timestamps null: false
    end
  end
end
