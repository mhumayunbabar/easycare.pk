class AddViewsCountToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :views_count, :integer, default: 0
  end
end
