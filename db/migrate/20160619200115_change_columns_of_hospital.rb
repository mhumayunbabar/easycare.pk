class ChangeColumnsOfHospital < ActiveRecord::Migration
  def change
  	change_column :hospitals, :lat, :decimal, :precision => 10, :scale => 8
  	change_column :hospitals, :long, :decimal, :precision => 11, :scale => 8  
  end
end
