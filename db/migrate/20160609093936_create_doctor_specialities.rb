class CreateDoctorSpecialities < ActiveRecord::Migration
  def change
    create_table :doctor_specialities do |t|
      t.integer :doctor_id
      t.integer :speciality_id

      t.timestamps null: false
    end
  end
end
