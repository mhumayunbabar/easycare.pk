class CreateTranzactions < ActiveRecord::Migration
  def change
    create_table :tranzactions do |t|
      t.integer :subject_id
      t.string :subject_type
      t.boolean :paid, default: false
      t.text :note
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
