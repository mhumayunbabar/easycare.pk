class CreateServiceProviders < ActiveRecord::Migration
  def change
    create_table :service_providers do |t|
      t.string :name
      t.string :email
      t.string :password
      t.text :description
      t.text :address
      t.decimal :long
      t.decimal :lat

      t.timestamps null: false
    end
  end
end
