class RemoveBloodTypeFromDonationRequest < ActiveRecord::Migration
  def change
    remove_column :donation_requests, :blood_type, :string
  end
end
