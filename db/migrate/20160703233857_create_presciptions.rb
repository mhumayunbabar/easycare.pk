class CreatePresciptions < ActiveRecord::Migration
	
  def change
    create_table :presciptions do |t|
      t.text :note
      t.integer :doctor_id
      t.integer :patient_id

      t.timestamps null: false
    end
  end
end
