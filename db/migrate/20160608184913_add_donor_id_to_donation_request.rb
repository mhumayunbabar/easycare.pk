class AddDonorIdToDonationRequest < ActiveRecord::Migration
  def change
    add_column :donation_requests, :donor_id, :integer
  end
end
