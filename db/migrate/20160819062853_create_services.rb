class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :title
      t.text :description
      t.float :charges
      t.integer :owner_id
      t.string :owner_type

      t.timestamps null: false
    end
  end
end
