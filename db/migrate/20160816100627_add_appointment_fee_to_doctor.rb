class AddAppointmentFeeToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :appointment_fee, :decimal
  end
end
