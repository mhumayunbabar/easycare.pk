class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.text :content
      t.string :subject_link
      t.integer :subject_id
      t.string :subject_type
      t.integer :owner_id
      t.string :owner_type

      t.timestamps null: false
    end
  end
end
