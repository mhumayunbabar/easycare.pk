class CreatePrescriptionItems < ActiveRecord::Migration
  def change
    create_table :prescription_items do |t|
      t.integer :prescription_id
      t.integer :medicine_id

      t.timestamps null: false
    end
  end
end
