class RemoveCityFromDonationRequest < ActiveRecord::Migration
  def change
    remove_column :donation_requests, :city, :integer
  end
end
