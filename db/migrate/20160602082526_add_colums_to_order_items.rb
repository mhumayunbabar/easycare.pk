class AddColumsToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :price, :float
    add_column :order_items, :amount, :integer
  end
end
