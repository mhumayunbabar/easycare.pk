class AddColumnsToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :bio, :text
    add_column :doctors, :experience, :text
    add_column :doctors, :education, :text
  end
end
