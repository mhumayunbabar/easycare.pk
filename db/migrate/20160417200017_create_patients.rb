class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.integer :gender
      t.date :date_of_birth
      t.string :email
      t.string :username
      t.string :password
      t.string :phone
      t.string :address
      t.integer :blood_type_id

      t.timestamps null: false
    end
    add_index :patients, :email, unique: true
    add_index :patients, :username, unique: true
  end
end
