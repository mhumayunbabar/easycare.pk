class AddNoteToDonor < ActiveRecord::Migration
  def change
    add_column :donors, :note, :text
  end
end
