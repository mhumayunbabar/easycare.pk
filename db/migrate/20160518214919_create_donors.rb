class CreateDonors < ActiveRecord::Migration
  def change
    create_table :donors do |t|
      t.string :mobile_no
      t.string :email
      t.string :name
      t.integer :gender
      t.integer :blood_type_id

      t.timestamps null: false
    end
  end
end
