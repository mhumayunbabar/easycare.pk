# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160823104148) do

  create_table "accounts", force: :cascade do |t|
    t.decimal  "balance"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true

  create_table "ahoy_events", force: :cascade do |t|
    t.integer  "visit_id"
    t.integer  "user_id"
    t.string   "name"
    t.text     "properties"
    t.datetime "time"
  end

  add_index "ahoy_events", ["name", "time"], name: "index_ahoy_events_on_name_and_time"
  add_index "ahoy_events", ["user_id", "name"], name: "index_ahoy_events_on_user_id_and_name"
  add_index "ahoy_events", ["visit_id", "name"], name: "index_ahoy_events_on_visit_id_and_name"

  create_table "appointments", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "state",      default: 0
  end

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.decimal  "lat"
    t.decimal  "long"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "context"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "blogger_id"
    t.integer  "state"
  end

  create_table "available_days", force: :cascade do |t|
    t.string   "day"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bloggers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.string   "username"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blood_types", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "blood_types", ["title"], name: "index_blood_types_on_title", unique: true

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.decimal  "lat"
    t.decimal  "long"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conditions", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "doctor_conditions", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "condition_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "doctor_patients", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doctor_procedures", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "procedure_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "doctor_specialities", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "speciality_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "name"
    t.integer  "gender"
    t.date     "date_of_birth"
    t.string   "email"
    t.string   "username"
    t.string   "password"
    t.string   "phone"
    t.string   "address"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "slot_duration"
    t.text     "bio"
    t.text     "experience"
    t.text     "education"
    t.string   "title_education"
    t.decimal  "appointment_fee"
    t.integer  "views_count",     default: 0
  end

  add_index "doctors", ["email"], name: "index_doctors_on_email", unique: true
  add_index "doctors", ["username"], name: "index_doctors_on_username", unique: true

  create_table "documents", force: :cascade do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "doc_type",           default: 0
  end

  create_table "donation_requests", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "status",        default: 0
    t.integer  "donor_id"
    t.integer  "blood_type_id"
    t.integer  "city_id"
  end

  create_table "donors", force: :cascade do |t|
    t.string   "mobile_no"
    t.string   "email"
    t.string   "name"
    t.integer  "gender"
    t.integer  "blood_type_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "note"
    t.integer  "city_id"
    t.integer  "area_id"
  end

  create_table "hospitals", force: :cascade do |t|
    t.string   "name"
    t.decimal  "lat",        precision: 10, scale: 8
    t.decimal  "long",       precision: 11, scale: 8
    t.integer  "city_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "medicines", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.text     "description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "order_item_id"
    t.integer  "item_code"
    t.integer  "state"
    t.float    "purchase_price"
    t.string   "manufacturer_name"
  end

  create_table "notifications", force: :cascade do |t|
    t.text     "content"
    t.string   "subject_link"
    t.integer  "subject_id"
    t.string   "subject_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "read",         default: false
  end

  create_table "order_items", force: :cascade do |t|
    t.integer  "item_id"
    t.string   "item_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float    "price"
    t.integer  "amount"
    t.integer  "order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string   "charges"
    t.text     "note"
    t.string   "address"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "area_id"
    t.integer  "pharmacy_id"
    t.integer  "status"
    t.integer  "payment_method"
    t.string   "phone"
  end

  create_table "patients", force: :cascade do |t|
    t.string   "name"
    t.integer  "gender"
    t.date     "date_of_birth"
    t.string   "email"
    t.string   "username"
    t.string   "password"
    t.string   "phone"
    t.string   "address"
    t.integer  "blood_type_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "doctor_id"
  end

  add_index "patients", ["email"], name: "index_patients_on_email", unique: true
  add_index "patients", ["username"], name: "index_patients_on_username", unique: true

  create_table "pharmacies", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.float    "lat"
    t.float    "long"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "email"
    t.string   "password"
    t.string   "username"
  end

  create_table "prescription_items", force: :cascade do |t|
    t.integer  "prescription_id"
    t.integer  "medicine_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "prescriptions", force: :cascade do |t|
    t.text     "note"
    t.integer  "doctor_id"
    t.integer  "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "procedures", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "service_providers", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password"
    t.text     "description"
    t.text     "address"
    t.decimal  "long"
    t.decimal  "lat"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.float    "charges"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 100, null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at"

  create_table "slots", force: :cascade do |t|
    t.datetime "start_time"
    t.integer  "appointment_id"
    t.integer  "doctor_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "stub",           default: false
  end

  create_table "specialities", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "stocks", force: :cascade do |t|
    t.integer  "pharmacy_id"
    t.integer  "medicine_id"
    t.integer  "amount"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["context"], name: "index_taggings_on_context"
  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
  add_index "taggings", ["tag_id"], name: "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context"
  add_index "taggings", ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy"
  add_index "taggings", ["taggable_id"], name: "index_taggings_on_taggable_id"
  add_index "taggings", ["taggable_type"], name: "index_taggings_on_taggable_type"
  add_index "taggings", ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type"
  add_index "taggings", ["tagger_id"], name: "index_taggings_on_tagger_id"

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true

  create_table "transactions", force: :cascade do |t|
    t.decimal  "amount"
    t.text     "note"
    t.boolean  "paid",       default: false
    t.string   "subject"
    t.integer  "account_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "tranzactions", force: :cascade do |t|
    t.integer  "subject_id"
    t.string   "subject_type"
    t.boolean  "paid",         default: false
    t.text     "note"
    t.decimal  "amount"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "type"
    t.string   "email"
    t.string   "username"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["username"], name: "index_users_on_username", unique: true

  create_table "visits", force: :cascade do |t|
    t.string   "visit_token",      limit: 100
    t.string   "visitor_token"
    t.string   "ip"
    t.text     "user_agent"
    t.text     "referrer"
    t.text     "landing_page"
    t.integer  "user_id"
    t.string   "referring_domain"
    t.string   "search_keyword"
    t.string   "browser"
    t.string   "os"
    t.string   "device_type"
    t.integer  "screen_height"
    t.integer  "screen_width"
    t.string   "country"
    t.string   "region"
    t.string   "city"
    t.string   "postal_code"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.string   "utm_source"
    t.string   "utm_medium"
    t.string   "utm_term"
    t.string   "utm_content"
    t.string   "utm_campaign"
    t.datetime "started_at"
  end

  add_index "visits", ["user_id"], name: "index_visits_on_user_id"
  add_index "visits", ["visit_token"], name: "index_visits_on_visit_token", unique: true

end
