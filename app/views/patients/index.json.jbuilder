json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :gender, :date_of_birth, :email, :username, :password, :phone, :address, :blood_type_id
  json.url patient_url(patient, format: :json)
end
