json.array!(@doctors) do |doctor|
  json.extract! doctor, :id, :name, :gender, :date_of_birth, :email, :username, :password, :phone, :address
  json.url doctor_url(doctor, format: :json)
end
