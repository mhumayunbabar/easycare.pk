class Usermailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.usermailer.contact.subject
  #
  def contact data
    @data = data
    # mail to: "easycare.pk@gmail.com"
    mail to: "humayun.babar.2010@gmail.com", subject: "Ali is Experimenting"
  end

  def test_email data
    @data = data
    mail to: "humayun.babar.2010@gmail.com", subject: "Ali is Experimenting"
  end

  def notify_admin doctor, patient, appointment
    @doctor = doctor
    @patient = patient
    @appointment = appointment
    
    mail to: "alii.maqsood95@gmail.com", subject: "New Appointment"
    mail to: "humayun.babar.2010@gmail.com", subject: "New Appointment"
  end
end
