class DoctorsController < ApplicationController
  before_action :authenticate_doctor, except: [:login ,:session_login]
  before_action :update_notifications, except: [:login ,:session_login, :via_n_appointments]
  before_action :set_doctor, only: [:show, :edit, :update, :destroy]
  require 'date'

  def add_new_condition
    for i in 0..params[:conditions].size-1
      current_doctor.doctor_conditions.create(doctor_id: current_doctor.id, condition_id: params[:conditions][i])
    end
    redirect_to :back
  end

  def new_condition
    @conditions = Condition.all
  end

  def conditions
    @conditions = current_doctor.doctor_conditions
  end

  def via_n_appointments
    unread = current_doctor.notifications.where(read: false, subject_type: 'Appointment').map(&:id)
    
    unread.each do |id|
      Notification.update(id, read: true)
    end
    render 'appointments'
  end

  def update_notifications
    @message = []
    @notifications = current_doctor.notifications.where(read: false, subject_type: 'Appointment').order(created_at: :desc)
    if @notifications.count != 0
      @message.push('You have ' + @notifications.count.to_s + ' new appointments.')
    end
    @notifications = current_doctor.notifications.where(read: false, subject_type: 'View').order(created_at: :desc)
    if @notifications.count != 0
      @message.push( @notifications.count.to_s + ' people viewed your profile. Total views: ' + current_doctor.views_count.to_s)
      @notifications.each do |n|
        Notification.update(n.id, read: true)
      end
    end
  end

  def update_information
    Doctor.update(current_doctor.id, bio: params[:bio], experience: params[:experience], education: params[:education], title_education: params[:title_education])
    flash[:success] = "Information updated successfully"
    redirect_to :back
  end

  def mark_appointment_paid
    Tranzaction.update(params[:tranzaction_id], note: params[:note], paid: true)
    redirect_to :back
  end

  def mark_paid
    @tranzaction = Appointment.find(params[:appointment_id]).tranzaction

  end

  def accept_appointment
    Appointment.update(params[:appointment_id], state: 1)
    redirect_to :back
  end

  def reject_appointment
    Appointment.update(params[:appointment_id], state: 2)
    redirect_to :back
  end

  def dashboard
    
  end

  def add_new_picture
    if current_doctor.document.present?
      current_doctor.document.update(image: params[:image])
    else
      current_doctor.create_document(image: params[:image])
    end
    redirect_to :back
  end

  def change_picture

  end

  def settings

  end

  def prescription_details
    medicine_ids = Prescription.find(params[:prescription_id]).prescription_items.select("medicine_id")
    @medicines = []
    medicine_ids.each do |medicine|
      @medicines.push(Medicine.find(medicine.medicine_id))
    end
    @prescription = Prescription.find(params[:prescription_id])
  end

  def prescriptions
    @prescriptions = current_doctor.prescriptions
  end

  def new_prescription
    @patients = Patient.all
    @medicines = Medicine.all
  end

  def add_new_prescription
    prescription = current_doctor.prescriptions.create(patient_id: params[:patient], note: params[:note])

    for i in 0..params[:medicines].size-1
      prescription.prescription_items.create(medicine_id: params[:medicines][i])
    end

    flash[:success_prescription] = "Prescription Added Successfully"
    redirect_to :back
  end

  def get_slots
    date = params[:date]
    if !date.present?
      respond_to do |format|
         format.json { render :json => @empty_slots }
      end
      return
    end
    date = Date.strptime(date, "%m/%d/%Y")
    date = date.to_datetime
    
    #current_doctor.verify_day date --verifies if the doctor has set available for the given day

    slots = current_doctor.slots.where(:start_time => date .. date.end_of_day)
    @empty_slots = current_doctor.generate_empty_slots date
    
    respond_to do |format|
       format.json { render :json => @empty_slots }
    end
  end

  def availability_settings
    @days = current_doctor.available_days
    @duration = current_doctor.slot_duration

  end

  def set_availability_details

    if (params[:duration].present?)
      Doctor.update(current_doctor.id, slot_duration: params[:duration])
    end
    if params[:amount].present?
      Doctor.update(current_doctor.id, appointment_fee: params[:amount])
    end
    if params[:mon].present?
      day = current_doctor.available_days.where(day: "Monday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Monday")
      end
      day.set_start_time params[:mon_start_time]
      day.set_end_time params[:mon_end_time]
    else
      day = current_doctor.available_days.where(day: "Monday").first
      if day.present?
        day.delete
      end 
    end
    if params[:tue].present?
      day = current_doctor.available_days.where(day: "Tuesday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Tuesday")
      end

      day.set_start_time params[:tue_start_time]
      day.set_end_time params[:tue_end_time]
    else
      day = current_doctor.available_days.where(day: "Tuesday").first
      if day.present?
        day.delete
      end 

    end
    if params[:wed].present?
      day = current_doctor.available_days.where(day: "Wednesday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Wednesday")
      end
      day.set_start_time params[:wed_start_time]
      day.set_end_time params[:wed_end_time]
    else
      day = current_doctor.available_days.where(day: "Wednesday").first
      if day.present?
        day.delete
      end 
    end
    if params[:thur].present?
      day = current_doctor.available_days.where(day: "Thursday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Thursday")
      end
      day.set_start_time params[:thur_start_time]
      day.set_end_time params[:thur_end_time]
    else
      day = current_doctor.available_days.where(day: "Thursday").first
      if day.present?
        day.delete
      end 
    end
    if params[:fri].present?
      day = current_doctor.available_days.where(day: "Friday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Friday")
      end
      day.set_start_time params[:fri_start_time]
      day.set_end_time params[:fri_end_time]
    else
      day = current_doctor.available_days.where(day: "Friday").first
      if day.present?
        day.delete
      end 
    end
    if params[:sat].present?
      day = current_doctor.available_days.where(day: "Saturday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Saturday")
      end
      day.set_start_time params[:sat_start_time]
      day.set_end_time params[:sat_end_time]
    else
      day = current_doctor.available_days.where(day: "Saturday").first
      if day.present?
        day.delete
      end 
    end
    if params[:sun].present?
      day = current_doctor.available_days.where(day: "Sunday").first 
      if !day.present?
        day = current_doctor.available_days.create(day: "Sunday")
      end
      day.set_start_time params[:sun_start_time]
      day.set_end_time params[:sun_end_time]
    else
      day = current_doctor.available_days.where(day: "Sunday").first
      if day.present?
        day.delete
      end 

    end
    redirect_to :back
  end

  def specialities
    doctor_specialities = current_doctor.doctor_specialities 
    @specialities = []
    if doctor_specialities.present?
      doctor_specialities.each do |doctor_speciality|
        @specialities.push(Speciality.find(doctor_speciality.speciality_id))
      end
    end
  end

  def add_speciality
    #send those specialities that hasn't been added by the doctor
    @specialities = Speciality.all
  end

  def add_new_speciality
    current_doctor.doctor_specialities.create(doctor_id: current_doctor.id, speciality_id: params[:specialities])
    redirect_to :back
  end

  def procedures
    doctor_procedures = current_doctor.doctor_procedures 
    @procedures = []
    
    if doctor_procedures.present?
      doctor_procedures.each do |doctor_procedure|
        @procedures.push(Procedure.find(doctor_procedure.procedure_id))
      end
    end
  end

  def add_procedure
    #send those procedures that hasn't been added by the doctor
    @procedures = Procedure.all
  end

  def add_new_procedure
    params[:procedures].each do |id|
      current_doctor.doctor_procedures.create(doctor_id: current_doctor.id, procedure_id: id)
    end
    redirect_to :back
  end

  # GET /doctors
  # GET /doctors.json
  def index
    @doctors = Doctor.all
  end

  # GET /doctors/1
  # GET /doctors/1.json
  def show
  end



  def appointments
  end
  
  def new_appointment
    @appointment = Appointment.new
  end

  def new_stub_appointment

  end

  def add_new_stub_appointment
    
    for i in 0..params[:selected_slots].size-1
      date_time = Date.strptime(params[:appointment_date], "%m/%d/%Y").to_datetime + Time.parse(params[:selected_slots][i]).seconds_since_midnight.seconds
      current_doctor.slots.create(start_time: date_time, stub: true)
    end
    flash[:success_appointment] = "Slots marked busy!"
    redirect_to :back
  end
  
  def create_appointment
    date_time = Date.strptime(params[:appointment_date], "%m/%d/%Y").to_datetime + Time.parse(params[:selected_slot]).seconds_since_midnight.seconds
    appointment = current_doctor.appointments.create(patient_id: params[:patient], state: 1)
    appointment.create_slot(doctor_id: current_doctor.id, start_time: date_time)
    appointment.notify_the_admin
    flash[:success_appointment] = "Appointment Created for " + Patient.find(params[:patient]).name + "!"
    redirect_to :back
  end
  
  def login
  end

  # GET /doctors/new
  def new
    @doctor = Doctor.new
  end

  # GET /doctors/1/edit
  def edit
  end

  # POST /doctors
  # POST /doctors.json
  def create
    
    @doctor = Doctor.new(doctor_params)

    respond_to do |format|
      if @doctor.save
         format.js { render :create, locals: { alert: "Doctor Added Succesfully"} }
      end
    end
  end

  # PATCH/PUT /doctors/1
  # PATCH/PUT /doctors/1.json
  def update
    respond_to do |format|
      if @doctor.update(doctor_params)
        format.html { redirect_to @doctor, notice: 'Doctor was successfully updated.' }
        format.json { render :show, status: :ok, location: @doctor }
      else
        format.html { render :edit }
        format.json { render json: @doctor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctors/1
  # DELETE /doctors/1.json
  def destroy
    @doctor.destroy
    respond_to do |format|
      format.html { redirect_to doctors_url, notice: 'Doctor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

   # newly added functions
  def session_login
    @doctor = Doctor.find_by(email: params[:email])
      if @doctor.blank? 
         redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif @doctor.password != params[:password]
            redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif !@doctor.blocked?
      
        set_session @doctor
        redirect_to({ :action=>'dashboard' }, notice: 'Welcome to admin panel.')
        return

     else
      redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
      return 
     end
  end

  def logout
    reset_session
    redirect_to root_path
  end
  
  def destroy_session 
    reset_session
  end

    def set_session admin
     session[:doctor] = admin.id
    end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor
      @doctor = Doctor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_params
      params[:date_of_birth] = Date.strptime(params[:date_of_birth], '%m/%d/%Y') 
      params.permit(:name, :gender, :email, :lat, :long, :password, :phone, :address, :date_of_birth)
    end

    def authenticate_doctor
     if session[:doctor].blank? && session[:admin].blank?
        redirect_to( login_doctors_path, notice: 'Please login to conitnue.') 
     end
  end  
end
