class PatientsController < ApplicationController
  before_action :authenticate_patient, except: [:login ,:session_login, :new, :create]
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  require 'date'

  def prescription_details
    medicine_ids = Prescription.find(params[:prescription_id]).prescription_items.select("medicine_id")
    @medicines = []
    medicine_ids.each do |medicine|
      @medicines.push(Medicine.find(medicine.medicine_id))
    end
    @prescription = Prescription.find(params[:prescription_id])
  end

  def prescriptions
    @prescriptions = current_patient.prescriptions
  end

  def logout
    reset_session
    redirect_to root_path
  end
  

  def get_slots
    date = params[:date]
    date = Date.strptime(date, "%m/%d/%Y")
    date = date.to_datetime
    
    #current_doctor.verify_day date --verifies if the doctor has set available for the given day
    current_doctor = Doctor.find(params[:doctor_id])
    slots = current_doctor.slots.where(:start_time => date .. date.end_of_day)
    @empty_slots = current_doctor.generate_empty_slots date
    
    respond_to do |format|
       format.json { render :json => @empty_slots }
    end
  end
  def appointments
    @appointments = Appointment.where(patient_id: current_patient.id)
  end

  def request_appointment
    @doctors = Doctor.all
  end

  def create_new_appointment
    date_time = Date.strptime(params[:appointment_date], "%m/%d/%Y").to_datetime + Time.parse(params[:selected_slot]).seconds_since_midnight.seconds
    appointment = current_patient.appointments.create(doctor_id: params[:doctor_id], state: 1)
    appointment.create_slot(doctor_id: params[:doctor_id], start_time: date_time)
    notification_message = 'A Patient booked an appointment for ' + params[:appointment_date] + ', at ' + params[:selected_slot]
    doctor = Doctor.find(params[:doctor_id])
    doctor.notifications.create(subject: appointment, content: notification_message)
    appointment.notify_the_admin
    flash[:success] = "Request submitted to Dr. " + doctor.name

    redirect_to :back
  end

  # GET /patients
  # GET /patients.json
  def index
    @patients = Patient.all
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
  end

  # GET /patients/new
  def new
    @patient = Patient.new
  end

  # GET /patients/1/edit
  def edit
  end

  # POST /patients
  # POST /patients.json
  def create
    #byebug
    if current_doctor.present?
      @patient = current_doctor.patients.create(patient_params)
    else
      @patient = Patient.create(patient_params)
    end
        
    if @patient.save
      flash[:success] = "Patient successfully saved"
    else
      flash[:error] = "Patient not saved. Use unique username or email"
    end
    redirect_to :back
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    respond_to do |format|
      if @patient.update(patient_params)
        format.html { redirect_to @patient, notice: 'Patient was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :edit }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    respond_to do |format|
      format.html { redirect_to patients_url, notice: 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

    def session_login
    @patient = Patient.find_by(email: params[:email])
      if @patient.blank? 
         redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif @patient.password != params[:password]
            redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif !@patient.blocked?
      
        set_session @patient
        redirect_to({ :action=>'dashboard' }, notice: 'Welcome to your dashboard.')
        return

     else
      redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
      return 
     end
  end

  def destroy_session 
    reset_session 
    redirect_to login_patients_path
  end

  def set_session patient
    session[:patient] = patient.id
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient
      @patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_params
      params.permit(:name, :gender, :date_of_birth, :blood_type_id, :email, :username, :password, :phone, :address)
    end
  def authenticate_patient
     if session[:patient].blank? && session[:patient].blank?
        redirect_to( login_patients_path, notice: 'Please login to conitnue.') 
     end
  end 
end
