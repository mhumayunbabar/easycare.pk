class WelcomeController < ApplicationController
   before_action :check_user, only: [:index]
  
  def index
  end

  def about
  end

  def login
  end

  def help
  end

  def faqs
  end

  def contact_us
  end

  private
  def check_user
     if session[:admin].present? 
        redirect_to dashboard_admins_path
      elsif session[:doctor].present?
        redirect_to dashboard_doctors_path
      else
        redirect_to login_doctors_path
     end
  end  
  
end
