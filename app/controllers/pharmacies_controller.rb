class PharmaciesController < ApplicationController
  before_action :authenticate_pharmacy, except: [:login ,:session_login]
  before_action :set_pharmacy, only: [:show, :edit, :update, :destroy]


  def stock

  end

  def add_stock
    @medicines = Medicine.all
    respond_to do |format|
         format.js 
    end
  end

  def add_new_stock
    stock = Stock.where(medicine_id: params[:medicine_id], pharmacy_id: current_pharmacy.id)
    if stock.present?
      quantity = stock.first.amount + params[:quantity].to_i
      Stock.update(stock.first.id, amount: quantity)
    else
      Stock.create(pharmacy_id: current_pharmacy.id, medicine_id: params[:medicine_id], amount: params[:quantity])
    end
    flash[:success] = "Stock added successfully"
    redirect_to :back
  end


  def index
    @pharmacy = Pharmacy.all
  end

  # GET /pharmacies/1
  def show
  end
  
  def login
  end

  def logout
    reset_session
    redirect_to root_path
  end
  

  # GET /pharmacies/new
  def new
    @pharmacy = Pharmacy.new
  end

  # GET /pharmacies/1/edit
  def edit
  end

  # POST /pharmacies
  
  def create 
    @pharmacy = Pharmacy.create(name: params[:name], email: params[:email], username: params[:username], password: params[:password], lat: params[:lat], long: params[:long], address: params[:address])
    redirect_to :back
  end
 
  # PATCH/PUT /pharmacies/1
  def update
    respond_to do |format|
      if @pharmacy.update(pharmacy_params)
        format.html { redirect_to @doctor, notice: 'Pharmacy was successfully updated.' }
        format.json { render :show, status: :ok, location: @pharmacy }
      else
        format.html { render :edit }
        format.json { render json: @pharmacy.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_status
    Order.update(params[:order_id], :status => ORDER_STATUS[:delivered])
    redirect_to :back
  end

  # DELETE /pharmacies/1
  def destroy
    @pharmacy.destroy
    respond_to do |format|
      format.html { redirect_to pharmacy_url, notice: 'Pharmacy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

   # newly added functions
  def session_login
    @pharmacy = Pharmacy.find_by(email: params[:email])
      if @pharmacy.blank? 
         redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif @pharmacy.password != params[:password]
            redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif !@pharmacy.blocked?
      
        set_session @pharmacy
        redirect_to({ :action=>'dashboard' }, notice: 'Welcome to your dashboard.')
        return

     else
      redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
      return 
     end
  end

    def destroy_session 
      reset_session 
      redirect_to login_pharmacies_path
    end

    def set_session logger
     session[:pharmacy] = logger.id
    end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pharmacy
      @pharmacy = Pharmacy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.

    def authenticate_pharmacy
      
     if session[:pharmacy].blank? && session[:admin].blank?
        redirect_to( login_pharmacies_path, notice: 'Please login to conitnue.') 
     end
  end  
end
