class BloggersController < ApplicationController
  before_action :authenticate_blogger, except: [:login ,:session_login]
  before_action :set_blogger, only: [:show, :edit, :update, :destroy]

  def new_article
    @article = Article.new
    @tags = ActsAsTaggableOn::Tag.all
  end

  def create_new_article
    @article = current_blogger.articles.create(title: params[:title], context: params[:content], state: 0)
    @article.documents.create(image: params[:image])
    for i in 0..params[:tags].size-1
      @article.tag_list.add(params[:tags][i])
    end
    @article.save
    flash[:success] = "Your article has been saved. It will be published shortly."
    redirect_to dashboard_bloggers_path(current_blogger)
    
  end

  def dashboard
    
  end

  def session_login
    @blogger = Blogger.find_by(email: params[:email])
      if @blogger.blank? 
         redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif @blogger.password != params[:password]
            redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif !@blogger.blocked?
      
        set_session @blogger
        redirect_to({ :action=>'dashboard' }, notice: 'Welcome to your dashboard.')
        return

     else
      redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
      return 
     end
  end

  def logout
    reset_session
    redirect_to root_path
  end
  

  def destroy_session 
    reset_session 
    redirect_to login_bloggers_path
  end

  def set_session blogger
    session[:blogger] = blogger.id
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blogger
      @blogger = blogger.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blogger_params
      params.permit(:name, :email, :username, :password)
    end

    def authenticate_blogger
     if session[:blogger].blank? && session[:admin].blank?
        redirect_to( login_bloggers_path, notice: 'Please login to conitnue.') 
     end
  end 
end
