class ApiController < ApplicationController

  def verify_email
    email = params[:email]
    if Doctor.find_by_email(email).present? 
      render plain: 'false'
      return
    elsif Patient.find_by_email(email).present? 
      render plain: 'false'
      return
    else
      render plain: 'true'
    end
  end

  def get_patients
    draw = params[:draw]
    patients = current_doctor.patients
    if params[:search][:value].present?
      patients = patients.search(params[:search][:value])
    end
    total_count = patients.count
    page = params[:start].to_i / params[:length].to_i + 1
    patients = patients.paginate(:page => page, :per_page => params[:length])
    respond_to do |format|
      format.json do
        render json: {"data": patients, 
                      "draw": draw ,
                      "recordsTotal": total_count,
                      "recordsFiltered": patients.count}
      end
    end
  end

  def get_stock
    draw = params[:draw]
    stocks = current_pharmacy.stocks
    #binding.pry
    if params[:search][:value].present?
      stocks = stocks.search(params[:search][:value])
    end
    total_count = stocks.count
    page = params[:start].to_i / params[:length].to_i + 1
    stocks = stocks.paginate(:page => page, :per_page => params[:length])
    data = stocks.collect do |stock| 
      medicine = Medicine.where(id: stock.medicine_id)
      { name: medicine.first.name,
        price: medicine.first.price,
        description: medicine.first.description,
        quantity: stock.amount
      }
    end 
    #binding.pry
    respond_to do |format|
      format.json do
        render json: {"data": data,
                    "draw": draw ,
            "recordsTotal": total_count,
            "recordsFiltered": stocks.count}
      end
    end
  end

  def get_medicines
    draw = params[:draw]
    params[:q] = params[:search][:value]
    medicines = Medicine.all
    if params[:q].present?
      medicines = Medicine.ransack({ "name_cont" => params[:q]})
      medicines = medicines.result(distinct: true)
    end
    total_count = medicines.count
    page = params[:start].to_i / params[:length].to_i + 1
    medicines = medicines.paginate(:page => page, :per_page => params[:length])
    respond_to do |format|
      format.json do
        render json: {"data": medicines, 
                      "draw": draw ,
                      "recordsTotal": total_count,
                      "recordsFiltered": medicines.count}
      end
    end
  end

  def get_orders
    draw = params[:draw]
    orders = current_pharmacy.orders

    if params[:search][:value].present?
      orders = orders.search(params[:search][:value])
    end
    total_count = orders.count
    page = params[:start].to_i / params[:length].to_i + 1
    orders = orders.paginate(:page => page, :per_page => params[:length])
    data = orders.collect do |order| 
      if order.payment_method == 1
        payment = "Cash on Delivery"
      else
        payment = "Easy Paisa"
      end

      if order.status == 1
        stat = "Pending"
      else
        stat = "Delivered"
      end

      { id: order.id,
        area: order.area.try(:name),
        charges: order.charges,
        address: order.address,
        payment_method: payment,
        status: stat
      }
    end 
    
    respond_to do |format|
      format.json do
        render json: {"data": data,
                    "draw": draw ,
            "recordsTotal": total_count,
            "recordsFiltered": orders.count}
      end
    end
  end

  def get_appointments
    draw = params[:draw]
    appointments = current_doctor.appointments.order(created_at: :desc)
    if params[:search][:value].present?
      appointments = appointments.search(params[:search][:value])
    end
    total_count = appointments.count
    page = params[:start].to_i / params[:length].to_i + 1
    appointments = appointments.paginate(:page => page, :per_page => params[:length])
    respond_to do |format|
      format.json do
        render json: {"data": appointments, 
                      "draw": draw ,
                      "recordsTotal": total_count,
                      "recordsFiltered": appointments.count}
      end
    end
  end
end
