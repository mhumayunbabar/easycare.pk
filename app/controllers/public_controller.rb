class PublicController < ApplicationController
  protect_from_forgery except: :add_medicine
  before_action :announce, only: [:patient_registration, :pharmacy, :appointment]
  before_action :empty_announce, except: [:patient_registration, :pharmacy, :appointment]

  include ActionView::Helpers::TextHelper
  require 'date'
  require 'will_paginate/array'

  def doctor_details
    @doctor = Doctor.find(params[:doctor_id])
    Doctor.update(@doctor.id, views_count: @doctor.views_count + 1)
    @doctor.notifications.create(content: 'Somebofy viewed your profile', subject_type: 'View', subject_id: -1)
  end

  def verifyforzoho

  end

  def search 
    @specialities = Speciality.all
    @procedures = Procedure.all
    @conditions = Condition.all
  end

  def search_doctors
    @doctors = []
    if params[:procedure_id].present?
      @doctors = Doctor.find(DoctorProcedure.where(procedure_id: params[:procedure_id]).map(&:doctor_id))
    end
    if params[:speciality_id].present?
      doctors_s = Doctor.find(DoctorSpeciality.where(speciality_id: params[:speciality_id]).map(&:doctor_id))
      #Doctor.joins(:doctor_specialities).where("'doctor_specialities.speciality_id' = ?", params[:speciality_id])
      if params[:procedure_id].present?
        @doctors = @doctors & doctors_s
      else
        @doctors = doctors_s
      end
    end
    if params[:condition_id].present?
      doctors_c = Doctor.find(DoctorCondition.where(condition_id: params[:condition_id]).map(&:doctor_id))
      #Doctor.joins(:doctor_specialities).where("'doctor_specialities.speciality_id' = ?", params[:speciality_id])
      if params[:speciality_id].present? || params[:procedure_id].present?
        @doctors = @doctors & doctors_c
      else
        @doctors = doctors_c
      end
    end
    
    if @doctors.present?
      ids = @doctors.map(&:id)
      @doctors = Doctor.find(ids).paginate(:page => params[:page])
      flash[:error] = ""
    else
      flash[:error] = "No Doctor Found"
    end
    
    @specialities = Speciality.all
    @procedures = Procedure.all
    @conditions = Condition.all
    render "search"
  end

  def doctors
    @doctors = Doctor.all.paginate(:page => params[:page]).order(name: :asc)
    
  end

  def doctor_registration
    
  end

  def register_doctor
    date = Date.strptime(params[:date_of_birth], '%m/%d/%Y') 
    Doctor.create(name: params[:name], email: params[:email], password: params[:password], date_of_birth: date, gender: params[:gender], phone: params[:phone], address: params[:address], long: params[:long], lat: params[:lat])
    render plain: "Patient Registered Successfully"
  end

  def open_appointment
    @name = params[:name]
    @date = params[:appointment_date]
    @address = params[:address]
    @email = params[:email]
    @doctors = Doctor.all
    @selected_doctor = params[:doctor]
    render "appointment"
  end

  def contact_us

  end

  def get_slots
    date = params[:date]
    date = Date.strptime(date, "%m/%d/%Y")
    date = date.to_datetime
    
    #current_doctor.verify_day date --verifies if the doctor has set available for the given day
    current_doctor = Doctor.find(params[:doctor_id])

    @empty_slots = current_doctor.generate_empty_slots date
    
    respond_to do |format|
       format.json { render :json => @empty_slots }
    end
  end

  def medicine_details
    @medicine = Medicine.find(params[:medicine_id])
  end

  def login 
    
    @user = Admin.find_by(email: params[:email])
    if !@user.blank?
      #login admin and return
      if @user.password == params[:password]
        session[:admin] = @user.id
        redirect_to({ :controller=>'admins', :action=>'dashboard' })
        return
      else
        flash[:alert] = "The password you entered is incorrect."
        redirect_to :back
        return
      end
    end
    @user = Blogger.find_by(email: params[:email])
    if !@user.blank?
      #login admin and return
      if @user.password == params[:password]
        session[:blogger] = @user.id
        redirect_to({ :controller=>'bloggers', :action=>'dashboard' })
        return
      else
        flash[:alert] = "The password you entered is incorrect."
        redirect_to :back
        return
      end
    end
    @user = Doctor.find_by(email: params[:email])
    if !@user.blank?
      #login doctor and return  
      if @user.password == params[:password]
      
        session[:doctor] = @user.id
        redirect_to({ :controller=>'doctors', :action=>'dashboard' })
        return
      else
        flash[:alert] = "The password you entered is incorrect."
        redirect_to :back
        return
      end
    end
    @user = Pharmacy.find_by(email: params[:email])
    if !@user.blank?
      #login pharmacy and return  
      if @user.password == params[:password]
        session[:pharmacy] = @user.id
        redirect_to({ :controller=>'pharmacies', :action=>'dashboard' })
        return
      else
        flash[:alert] = "The password you entered is incorrect."
        redirect_to :back
        return
      end
    end
    @user = Patient.find_by(email: params[:email])
    if !@user.blank?
      #login doctor and return  
      if @user.password == params[:password]
        session[:patient] = @user.id
        redirect_to({ :controller=>'patients', :action=>'dashboard' })
        return
      else
        flash[:alert] = "The password you entered is incorrect."
        redirect_to :back
        return
      end
    end
    flash[:alert] = "The email address you entered is incorrect"

    redirect_to :back
  end

  def appointment
      @doctors = Doctor.all
  end

  def book_appointment
    date_time = Date.strptime(params[:appointment_date], "%m/%d/%Y").to_datetime + Time.parse(params[:selected_slot]).seconds_since_midnight.seconds
    patient = Patient.create(name: params[:name], phone: params[:phone], email: params[:email], blood_type_id: params[:blood_group], address: params[:address], gender: params[:gender], date_of_birth: params[:date_of_birth], password: params[:password], username: params[:username])
    appointment = patient.appointments.create(doctor_id: params[:doctor_id], state: 1)
    appointment.create_slot(doctor_id: params[:doctor_id], start_time: date_time)
    notification_message = 'A Patient booked an appointment for ' + params[:appointment_date] + ', at ' + params[:selected_slot]
    Doctor.find(params[:doctor_id]).notifications.create(subject: appointment, content: notification_message)
    appointment.notify_the_admin
    render plain: "Patient Registered Successfully"
  end

  def patient_registration
  end

  def register_patient
    Patient.create(name: params[:name], phone: params[:phone], email: params[:email], blood_type_id: params[:blood_group], address: params[:address], gender: params[:gender], date_of_birth: params[:date_of_birth], password: params[:password], username: params[:username])
    render plain: "Patient Registered Successfully"
    #redirect_to public_patient_registration_path, alert: "Thankyou for registering as a patient."
  end

  def healthy_living
    @articles = Article.where(state: BLOG_STATUS[:published]).paginate(:page => params[:page]).order(updated_at: :desc)
    @articles.each do |article|
      article.context = truncate(article.context, length: 250, seperator: ' ')
    end 
  end

  def blog_details
    @article = Article.find(params[:id])
  end

  def home
    if flash[:alert].present? 
      error = flash[:alert]
      reset_session
      flash[:alert] = error
    else
      reset_session
    end
    doctors = Doctor.where.not(long: nil, lat: nil).last(2)
    @doctors = []
    @doctor_ids = []
    doctors.each do |doctor|
      @doctors.push({lat: doctor.lat, lng: doctor.long})
      @doctor_ids.push(doctor.id)
    end
    session[:cart] = Array.new
    session[:quantity] = {}
  end

  def contact_form
    Usermailer.contact(params).deliver_now
    redirect_to :back, alert: "Thankyou for contacting us, we will get back to you as soon as possible."
  end

  def show_location
    @lat= params[:lat]
    @long= params[:long]
  end

  def add_medicine
    @medicine = Medicine.find(params[:medicine_id])

    session[:cart] = [] if session[:cart].nil? 
    session[:quantity] = {} if session[:quantity].nil?

    session[:cart].push(@medicine)
    session[:quantity][@medicine.id] = 1 #params[:quantity]
    respond_to do |format|
         format.js 
    end
  end

  def checkout
    @medicines = session[:cart]
    @charges = calculate_charges(@medicines)
    if @charges < 200
      flash[:error] = "Minimum amount of order is Rs. 200. Your amount was: Rs. " + @charges.to_s
      redirect_to :back
      return
    end
    if !session[:cart].present?
      flash[:error] = "Oops, looks like you have not selected any medicine to buy"
      redirect_to public_pharmacy_path
      return
    end
    
  end

  def place_order
    medicine_ids = params[:medicines]
    medicines = Array.new

    medicine_ids.each do |id|
      temp = Medicine.find(id)
      medicines.push(temp)
    end
    @charges = calculate_charges(medicines)
    order = Order.create(phone: params[:phone_number], address: params[:address], charges: @charges, payment_method: params[:payment_method], status: ORDER_STATUS[:pending])
    
    medicines.each do |medicine|
      order.order_items.create(item: medicine, amount: session[:quantity][medicine.id], price: medicine.price * session[:quantity][medicine.id].to_i)
    end

    session[:cart].clear
    session[:quantity].clear
    session[:order_id] = order.id
    redirect_to public_thank_you_path
    
  end

  def thank_you
     @order = Order.find(session[:order_id])
  end 

 

  def calculate_charges(medicines)
    counter = 0    
    if medicines.present?
      medicines.each do |medicine|
        counter = counter + medicine.price
      end
    end
    counter
  end

  def contact

  end

  def register_request_donation
    DonationRequest.create(name: params[:name], phone: params[:phone], email: params[:email], city_id: params[:city], blood_type_id: params[:blood_group])
    render plain: "Blood Request Registered"
    #redirect_to public_request_donation_path, alert: "Your blood donation request has been submitted"
  end

  def about

  end

  def privacy
  end

  def pharmacy
    @q = Medicine.ransack(params[:q])  
    if params[:q].present?
      if params[:q][:name_cont] != ""
        @events = @q.result(distinct: true).paginate(:page => params[:page])
        if @events.empty?
          flash[:error] = "No search results found"
        end
      end
    end
    @medicines = Medicine.first(6)
  end

  def help
  end

  def donor_registration
    
  end
  
  def register_donor
    Donor.create(name: params[:name], mobile_no: params[:phone], email: params[:email], blood_type_id: params[:blood_group], note: params[:note], city_id: params[:city])
    render plain: "Donor Registered Successfully"
    #redirect_to public_donor_registration_path, alert: "Thankyou for registering as a blood donor, People in need of blood will contact you."
  end
  
  def terms_and_conditions
  end

  private
    def announce
      flash[:announcement] = "This service is coming soon. Thank you for patience!"
    end

    def empty_announce
      flash[:announcement] = nil
    end

end
