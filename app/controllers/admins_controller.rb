class AdminsController < ApplicationController
  before_action :authenticate_admin, except: [:login,:session_login]
  before_action :set_admin, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, only: [:import_medicines_data] 
  # helper_method :current_admin #make this method available in views
  # GET /admins
  # GET /admins.json

  def donors
    @donors = Donor.all
  end

  def pharmacies
    @pharmacies = Pharmacy.all
  end

  def import_medicines_data
    Medicine.import(params[:file])
    redirect_to :back
  end

  def import_medicines
    respond_to do |format|
         format.js 
    end
  end

  def edit_hospital_location
    Hospital.update(params[:hospital_id], lat: params[:lat], long: params[:long])
    redirect_to :back
  end

  def edit_location
    @hospital = Hospital.find(params[:hospital_id])
  end

  def blogs
    @blogs = Article.all
    
  end

  def blog_content
    @blog = Article.find(params[:blog_id])
    
    respond_to do |format|
         format.js 
    end
  end

  def publish_blog
    Article.update(params[:blog_id], state: 1)
    
    flash[:success] = "Article published successfully"
    redirect_to blogs_admins_path 
  end

  def new_blogger
    respond_to do |format|
         format.js 
    end
  end

  def create_new_blogger
    Blogger.create(name: params[:name], username: params[:username], password: params[:password], email: params[:email])
    redirect_to :back    
  end


  def dashboard
    @visits_today = Visit.where(["started_at >= ?", Time.now.midnight-5.hour]).count

    @visits_last_weeek = Visit.where(["started_at >= :start_date AND started_at < :end_date", {start_date: Time.now.midnight-5.hour-7.day, end_date: Time.now.midnight-5.hour}]).count

  end

  def new_medicine
    respond_to do |format|
         format.js 
    end
  end

  def create_new_medicine
    Medicine.create(name: params[:name], item_code: params[:item_code], purchase_price: params[:purchase_price], description: params[:description], price: params[:price], state: params[:state], manufacturer_name: params[:manufacturer_name])
    if (params[:image].present?)
      @medicine = Medicine.last
      @medicine.create_document(image: params[:image])
    end
    redirect_to :back
  end

  def edit_medicine
    @medicine = Medicine.find(params[:medicine_id])
    respond_to do |format|
         format.js 
    end
  end

  def delete_medicine
    medicine = Medicine.find(params[:medicine_id])
    if medicine.document.present?
      medicine.document.delete
    end
    Medicine.delete(params[:medicine_id])
    respond_to do |format|
     format.js {render plain: "success"}
    end
  end

  def edit_medicine_data
    @medicine = Medicine.find(params[:medicine_id])
    Medicine.update(params[:medicine_id], :manufacturer_name => params[:manufacturer_name], :state => params[:state], :purchase_price => params[:purchase_price], :item_code => params[:item_code], :name => params[:name], :price => params[:price], :description => params[:description] )
    if params[:image].present?
      if @medicine.document.present?
        @medicine.document.delete
      end
      @medicine.create_document(image: params[:image])
    end
    redirect_to :back
  end


  def index
    @admins = Admin.all
  end

  # GET /admins/1
  # GET /admins/1.json
  def show
  end

  def new_blood_group
  end

  def donation_requests
    @donation_requests = DonationRequest.all
  end

  def assign_donation_request
    @donation_request = DonationRequest.find(params[:donation_request_id])
    #donor not shown of same city
    @donors = Donor.where(:blood_type_id => @donation_request.blood_type_id, city_id: @donation_request)
    respond_to do |format|
         format.js 
    end
  end

  def assign_donor
    
    DonationRequest.update(params[:donation_request_id], :donor_id => params[:donor_id], :status => 1)
    redirect_to :back
  end

  def add_new_blood_group
    BloodType.create(title: params[:type])
    respond_to do |format|
         format.js { render :add_new_blood_group, locals: { alert: "Blood Group Added Succesfully"} }
    end
  end

  def add_new_hospital
    Hospital.create(name: params[:name], city_id: params[:city], lat: params[:lat], long: params[:long])
    redirect_to :back
  end

  def cities
    @cities = City.all
  end

  def add_new_city
    City.create(name: params[:name], lat: params[:lat], long: params[:long])
    redirect_to :back
  end

  def procedures
    @procedures = Procedure.all
  end 
  
  def hospitals
    @hospitals = Hospital.all
  end 

  def conditions
    @conditions = Condition.all
  end 

  def specialities
    @sepcialities = Speciality.all
  end

  def order_details
    @order = Order.find(params[:order_id])
    @pharmacies = Pharmacy.all
    respond_to do |format|
         format.js 
    end
  end

  def assign_order
    Order.update(params[:order], :pharmacy_id => params[:pharmacy_id], :status => ORDER_STATUS[:assigned])
    redirect_to :back
  end

  def orders
    @orders = Order.all
  end

  def new_procedure
  
    respond_to do |format|
         format.js 
    end
  end

  def new_condition
  
    respond_to do |format|
         format.js 
    end
  end

  def create_new_procedure
      Procedure.create(title: params[:title], description: params[:description])
     redirect_to :back
  end

  def create_new_condition
      Condition.create(title: params[:title], description: params[:description])
     redirect_to :back
  end

  def new_speciality
  
    respond_to do |format|
         format.js 
    end
  end

  def create_new_speciality
      Speciality.create(title: params[:title], description: params[:description])
     redirect_to :back
  end

  # GET /admins/new
  def new
    @admin = Admin.new
  end

  # GET /admins/1/edit
  def edit
  end

  # POST /admins
  # POST /admins.json
  def create
    @admin = Admin.new(admin_params)

    respond_to do |format|
      if @admin.save
        format.html { redirect_to @admin, notice: 'Admin was successfully created.' }
        format.json { render :show, status: :created, location: @admin }
      else
        format.html { render :new }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admins/1
  # PATCH/PUT /admins/1.json
  def update
    respond_to do |format|
      if @admin.update(admin_params)
        format.html { redirect_to @admin, notice: 'Admin was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin }
      else
        format.html { render :edit }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admins/1
  # DELETE /admins/1.json
  def destroy
    @admin.destroy
    respond_to do |format|
      format.html { redirect_to admins_url, notice: 'Admin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  

  # newly added functions
  def session_login
    @admin = Admin.find_by(email: params[:email])
      if @admin.blank? 
         redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif @admin.password != params[:password]
            redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
         return
      elsif !@admin.blocked?
      
        set_session @admin
        redirect_to({ :action=>'dashboard' }, notice: 'Welcome to admin panel.')
        return

     else
      redirect_to({ :action=>'login' }, alert: 'Information you entered is wrong.') 
      return 
     end
  end


 def logout
    reset_session
    redirect_to root_path
  end
  
  
  def destroy_session 
    reset_session 
    redirect_to login_admins_path
  end

  private

    def set_session admin
     session[:admin] = admin.id
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = Admin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_params
      params.require(:admin).permit(:email, :password)
    end

   def authenticate_admin
     if session[:admin].blank? 
        redirect_to( login_admins_path, notice: 'Please login to conitnue.') 
     end
  end  
end
