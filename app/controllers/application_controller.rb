class ApplicationController < ActionController::Base

   # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
 
  def current_doctor
    Doctor.find(session[:doctor]) if session[:doctor].present? 
  end
  def current_pharmacy
    Pharmacy.find(session[:pharmacy]) if session[:pharmacy].present? 
  end
  def current_blogger
    Blogger.find(session[:blogger]) if session[:blogger].present? 
  end
  def current_patient
    Patient.find(session[:patient]) if session[:patient].present? 
  end

  helper_method :current_patient
  helper_method :current_blogger
  helper_method :current_doctor
  helper_method :current_pharmacy
end
