class Slot < ActiveRecord::Base
	belongs_to :doctor
	belongs_to :appointment, dependent: :destroy
end
