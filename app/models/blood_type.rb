class BloodType < ActiveRecord::Base
  has_many :patients
  has_many :donors
  has_many :donation_request
end
