class Patient < ActiveRecord::Base
  belongs_to :blood_type
  has_many :appointments
  has_many :prescriptions
  
  def blocked?
    false
  end

end
