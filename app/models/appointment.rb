class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :doctor
  has_one :slot
  has_one :tranzaction, as: :subject
  after_create :init_transaction
  def as_json(options={})
    super(options.merge({:include=> {:patient => {}},:methods => [:get_time, :get_state]}))
  end

  def init_transaction
    create_tranzaction(amount: doctor.appointment_fee)
  end

  def get_time
  	if slot.present? 
    	return slot.start_time.strftime("%m/%d/%Y at %I:%M%p")
    else
    	return "No Time Assigned"
    end
  end

  def get_state
    if state == 0
      #<%= link_to accept_appointment_doctor_path(current_doctor, appointment_id: " + id + "),  class: 'btn btn-success' %> Accept <% end %> &nbsp; 
      return ("<a href='/doctors/" + doctor.id.to_s + "/accept_appointment?appointment_id=" + id.to_s + "' class='btn btn-success'>Accept</a> &nbsp <a href='/doctors/" + doctor.id.to_s + "/reject_appointment?appointment_id=" + id.to_s + "' class='btn btn-danger'>Reject</a>")
      #<%= link_to reject_appointment_doctor_path(current_doctor, appointment_id: " + id + ")  class: 'btn btn-danger' %> Reject<% end %> "
    elsif state == 1
      if (tranzaction.present? && tranzaction.paid == true)
        return "Accepted and Fees Paid"
      else
        return "Accepted: " + "<a data-remote='true' href='/doctors/" + doctor.id.to_s + "/mark_paid?appointment_id=" + id.to_s + "' class='btn btn-info'>Mark Paid</a>"  
      end
    else
      return "Rejected"
    end
  end

  def notify_the_admin
    Usermailer.notify_admin(doctor, patient, self).deliver_later
  end
  
end
