class Doctor < ActiveRecord::Base
  has_many :appointments
  has_many :patients
  has_many :doctor_specialities
  has_many :doctor_procedures
  has_many :doctor_conditions
  has_many :available_days
  has_many :slots
  has_many :prescriptions
  has_one :account, as: :owner
  has_one :document, as: :owner
  has_many :notifications, as: :owner
  self.per_page = 8
  def blocked?
    false
  end

  def get_image
    if document.present?
      return document.image
    else
      return get_default_image
    end
  end

  def get_default_image
    if gender == 1
      return '/images/male_doctor.png'
    else
      return '/images/female_doctor.png'
    end
  end

  def generate_empty_slots date
  	duration = slot_duration.to_i
  	day_detail = available_days.where(day: date.strftime('%A')).first
    
    if !day_detail.present?
      return []
    end

  	start = day_detail.start_time
  	empty_slots = []

  	while start <= day_detail.end_time - duration.minutes
  		empty_slots.push(start)
  		start = start + duration.minutes
   	end

   	allotted_slots = slots.select(:start_time).where(:start_time => date .. date.end_of_day)
    #raise exception if empty_slots has zero count
    allotted_slots_times = []
    for i in 0..allotted_slots.size-1
      allotted_slots_times.push(allotted_slots[i].start_time.to_datetime.to_s(:time))
    end
    for i in 0..empty_slots.size-1
      empty_slots[i] = empty_slots[i].to_datetime.to_s(:time)
    end
   	empty_slots = empty_slots - allotted_slots_times

   	return empty_slots
  end

  def get_specialities
    specialities = ""
    if !doctor_specialities.present?
      return "No Specialities Mentioned"
    end
    doctor_specialities[0..1].each do |d|
      specialities += d.speciality.title + ", "
    end
    specialities = specialities[0..-3]
    specialities += " ..."
     
  end
  
  def get_procedures
    procedures = ""
    if !doctor_procedures.present?
      return "No Procedures Mentioned"
    end
    doctor_procedures[0..1].each do |d|
      procedures += d.procedure.title + ", "
    end

    procedures = procedures[0..-3]
    procedures += " ..."
  end

  def get_conditions
    conditions = ""
    if !doctor_conditions.present?
      return "No Conditions Mentioned"
    end
    doctor_conditions[0..1].each do |d|
      conditions += d.condition.title + ", "
    end
    conditions = conditions[0..-3]
    conditions += " ..."
  end

end
