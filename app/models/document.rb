class Document < ActiveRecord::Base
	belongs_to :owner, polymorphic: true, dependent: :destroy
	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/man.png"
  	validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
