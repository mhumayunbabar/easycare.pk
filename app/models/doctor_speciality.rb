class DoctorSpeciality < ActiveRecord::Base
	belongs_to :doctor, dependent: :destroy
	belongs_to :speciality, dependent: :destroy
end
