class Pharmacy < ActiveRecord::Base
	has_many :orders
	has_many :stocks

  def blocked?
    false
  end
end
