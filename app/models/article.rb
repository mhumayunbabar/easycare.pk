class Article < ActiveRecord::Base
	acts_as_taggable 
	acts_as_taggable_on :tags
	belongs_to :blogger
	has_many :documents, as: :owner
	self.per_page = 3
	def get_month_for_created_time
		month = created_at.month
		if month == 1
			return "Jan"
		elsif month == 2
			return "Feb"
		elsif month == 3
			return "Mar"
		elsif month == 4
			return "Apr"
		elsif month == 5
			return "May"
		elsif month == 6
			return "Jun"
		elsif month == 7
			return "Jul"
		elsif month == 8
			return "Aug"
		elsif month == 9
			return "Sep"
		elsif month == 10
			return "Oct"
		elsif month == 11
			return "Nov"
		elsif month == 12
			return "Dec"
		end	
	end

end
