class ServiceProvider < ActiveRecord::Base
	has_many :services, as: :owner
end
