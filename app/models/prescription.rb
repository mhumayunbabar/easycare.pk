class Prescription < ActiveRecord::Base
	has_many :prescription_items
	belongs_to :doctor
	belongs_to :patient
end
