class Donor < ActiveRecord::Base
	has_many :donation_requests
	belongs_to :blood_type
	belongs_to :city
end
