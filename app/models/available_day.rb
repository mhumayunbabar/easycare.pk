class AvailableDay < ActiveRecord::Base
	belongs_to :doctor

	def set_start_time time
		self.update(start_time: time.to_datetime)
	end

	def set_end_time time
		self.update(end_time: time.to_datetime)
	end

	def start_time_in_format
		start_time.strftime("%H:%M")
	end

	def end_time_in_format
		end_time.strftime("%H:%M")
	end
end
