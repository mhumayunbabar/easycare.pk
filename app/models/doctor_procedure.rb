class DoctorProcedure < ActiveRecord::Base
	belongs_to :doctor, dependent: :destroy
	belongs_to :procedure, dependent: :destroy
end
