class Medicine < ActiveRecord::Base
	has_many :stocks
	has_one :document, as: :owner
	has_many :order_items, as: :item
	has_many :prescription_items
	self.per_page = 9
	def self.import(file)
	  spreadsheet = open_spreadsheet(file)
	  header = spreadsheet.row(1)
	  (2..spreadsheet.last_row).each do |i|
	    row = Hash[[header, spreadsheet.row(i)].transpose]
	    medicine = new
	    medicine.name = row["Name"]
	    medicine.price = row["saleprice"]
	    medicine.purchase_price = row["purprice"]
	    medicine.item_code = row["aliasname"]
	    medicine.state = 0
	    medicine.manufacturer_name = row["name"]
	    medicine.save!
	  end
	end

	def self.active 
		where(state: 1)
	end

	def self.open_spreadsheet(file)
	  require 'roo'
	  case File.extname(file.original_filename)
	  when ".csv" then Roo::Csv.new(file.path, nil, :ignore)
	  when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	  when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	  else raise "Unknown file type: #{file.original_filename}"
	  end
	end
end
