class Notification < ActiveRecord::Base
	belongs_to :subject, polymorphic: true, dependent: :destroy
	belongs_to :owner, polymorphic: true, dependent: :destroy


end
