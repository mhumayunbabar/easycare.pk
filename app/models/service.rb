class Service < ActiveRecord::Base
	belongs_to :owner, polymorphic: true, dependent: :destroy
	has_many :documents, as: :owner
end
