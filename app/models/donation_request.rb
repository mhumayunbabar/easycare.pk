class DonationRequest < ActiveRecord::Base
	belongs_to :donor
	belongs_to :blood_type
	belongs_to :city
end
