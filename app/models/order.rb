class Order < ActiveRecord::Base
	belongs_to :pharmacy
	has_many :order_items
	belongs_to :area
end
