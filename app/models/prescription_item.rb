class PrescriptionItem < ActiveRecord::Base
	belongs_to :medicine
	belongs_to :prescription
end
