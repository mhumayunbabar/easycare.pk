class DoctorCondition < ActiveRecord::Base
	belongs_to :doctor, dependent: :destroy
	belongs_to :condition, dependent: :destroy
end
