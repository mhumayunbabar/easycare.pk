class Account < ActiveRecord::Base
	belongs_to :owner, polymorphic: true, dependent: :destroy
	has_many :tranzactions, as: :subject
end
