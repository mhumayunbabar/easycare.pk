module AdminsHelper
  def current_admin
    Admin.find(session[:admin]) if session[:admin].present? 
  end

end
